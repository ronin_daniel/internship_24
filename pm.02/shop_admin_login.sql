CREATE LOGIN ShopAdmin
WITH PASSWORD = 'shopadmin';

ALTER USER ShopAdmin
WITH LOGIN = ShopAdmin;