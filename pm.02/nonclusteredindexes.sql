create nonclustered index Good_Name
on Good (name ASC); 

create nonclustered index Customer_Name
on Customer (name ASC); 

create nonclustered index Shop_Name
on Shop (name ASC); 