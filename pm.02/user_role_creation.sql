CREATE LOGIN SteveManager 
WITH PASSWORD = 'steve';

CREATE USER SteveManager
FROM LOGIN SteveManager; 

CREATE ROLE ShopManager;

GRANT 
 SELECT, 
 INSERT, 
 UPDATE, 
 DELETE 
 ON Shop 
 TO ShopManager;
