---Procedure to update good's price
CREATE PROCEDURE UpdateGoodsPrice @idGood int, @price decimal(9,3)
AS
	UPDATE Good
	SET price = @price
	WHERE idGood = @idGood;

---Procedure to delete certain customer
CREATE PROCEDURE DeleteCustomer @idCustomer int
AS 
	DELETE Customer
	WHERE idCustomer = @idCustomer;
	
---Trigger to store deleted customers
ALTER TRIGGER OnDeleteCustomer
ON Customer
AFTER DELETE
AS 
	INSERT INTO DeletedCustomer 
	(idCustomer, name, phone)
	SELECT idCustomer, name, phone FROM deleted;

---Trigger to store deleted shops
CREATE TRIGGER OnDeleteShop
ON Shop
AFTER DELETE
AS 
	INSERT INTO DeletedShop
	(idShop, name, address, phone)
	SELECT idShop, name, address, phone FROM deleted;
