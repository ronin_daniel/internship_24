CREATE LOGIN CustomerManager
WITH PASSWORD = 'manager';

CREATE USER CustomerManager
FROM LOGIN CustomerManager;

GRANT SELECT, INSERT, UPDATE, DELETE ON Customer TO CustomerManager;