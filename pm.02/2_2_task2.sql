---Query for goods with shop names
SELECT
	g.idGood,
	g.name as "Good Name",
	g.price,
	s.name as "Shop Name"
FROM 
	Good as g
	JOIN
	Shop as s ON s.idShop = g.idShop;

---Query for customers with filled in phone
SELECT * FROM Customer as c
WHERE c.phone IS NOT NULL;

---DML query for dropping prices of goods more expensive than 1000
UPDATE Good
SET price -= 100
WHERE price > 1000;

---DML Query for stating null phone as default -
UPDATE Customer
SET phone = 
	CASE phone
	WHEN NULL THEN '-'
	ELSE phone
	END;

---Table query for changing Shop's phone default to -
ALTER TABLE Shop
ADD CONSTRAINT df_shop
DEFAULT '-' FOR phone;

---Table query for adding Good's description column
ALTER TABLE Good
ADD description varchar(100);