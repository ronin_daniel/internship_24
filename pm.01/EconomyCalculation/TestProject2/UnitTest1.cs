using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApp1.Models;

namespace TestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ProfitabilityAmountCalcTest()
        {
            double expenses = 1500;
            double price = 15;
            double costPrice = 7;

            double amountProf = 
                ProfitabilityCalculator
                .calcAmountProfitability(expenses, price, costPrice);
            Assert.AreEqual(amountProf, 187.5);
        }

        [TestMethod]
        public void ProfitabilityMoneyCalcTest()
        {
            double expenses = 1500;
            double price = 15;
            double costPrice = 7;

            double moneyProf = 
                ProfitabilityCalculator
                .calcMoneyProfitability(expenses, price, costPrice);
            Assert.AreEqual(moneyProf, 2812.5);
        }
    }
}
