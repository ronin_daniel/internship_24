﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Repositories;

namespace WindowsFormsApp1
{
    public partial class CalcForm : Form
    {
        public CalcForm()
        {
            InitializeComponent();
        }

        private async void calcButton_Click(object sender, EventArgs e)
        {
            try
            {
                double expenses = //получение затрат
                    Convert.ToDouble(expensesTextBox.Text);
                double price = //получение цены
                    Convert.ToDouble(priceTextBox.Text);
                double costPrice = //получение себестоимости
                    Convert.ToDouble(costPriceTextBox.Text);

                if (expenses <= 0 ||
                    price <= 0 ||
                    costPrice <= 0)
                {//проверка корректности данных
                    MessageBox.Show("Данные неверны");
                    return;
                }
                //расчёт рентабельности единиц товара
                double amount = await ProfitabilityCalculatorRepository
                    .calcAmountProfitability(expenses, price, costPrice);
                amountLabel.Text = amount.ToString();//размещение данных
                //расчёт рентабельности единиц товара
                double money = await ProfitabilityCalculatorRepository
                    .calcMoneyProfitability(expenses, price, costPrice);
                moneyLabel.Text = money.ToString();//размещение данных
            }
            catch
            {

            }
        }

        private void expensesTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void expensesTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void priceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void costPriceTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
    }
}
