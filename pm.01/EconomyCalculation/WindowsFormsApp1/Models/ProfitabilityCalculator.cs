﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
    public static class ProfitabilityCalculator
    {
        private static double amountProf;//новая переменная состояния

        //расчёт рентабельности в единицах товара
        public static double calcAmountProfitability(double expenses,//затраты
                                                     double price,//цена
                                                     double costPrice)//себестоимость
        {
            amountProf = Math.Round(expenses / (price - costPrice), 2);//формула расчёта
            return amountProf;
        }
        //расчёт рентабельности в денежных единицах
        public static double calcMoneyProfitability(double expenses,//затраты
                                                    double price,//цена
                                                    double costPrice)//себестоимость
        {
            double moneyProf;
            if (amountProf != null)
            {
                moneyProf = Math.Round(amountProf * price, 2);//формула расчёта
            }
            else
            {
                moneyProf = Math.Round(calcAmountProfitability(expenses, //формула расчёта
                                                      price,
                                                      costPrice) * price, 2);
            }
            return moneyProf;
        }
    }
}
