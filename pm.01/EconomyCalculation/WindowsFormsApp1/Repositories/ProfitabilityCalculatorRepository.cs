﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Models;

namespace WindowsFormsApp1.Repositories
{
    public static class ProfitabilityCalculatorRepository
    {
        public static async Task<double> calcAmountProfitability(double expenses,
                                                                 double price,
                                                                 double costPrice)
        {
            return await Task<double>.Run(() 
                => ProfitabilityCalculator
                .calcAmountProfitability(//расчёт рентабельности в единицах товара
                    expenses,   //затраты
                    price,      //цена
                    costPrice));//себестоимость
        }

        public static async Task<double> calcMoneyProfitability(double expenses,
                                                                double price,
                                                                double costPrice)
        {
            return await Task<double>.Run(()
                => ProfitabilityCalculator
                .calcMoneyProfitability(//расчёт рентабельности в денежных единицах
                    expenses,   //затраты
                    price,      //цена
                    costPrice));//себестоимость
        }
    }
}
